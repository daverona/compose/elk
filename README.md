# daverona/compose/elk

## Prerequisites

* `kibana.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/compose/traefik) out

## Quick Start

```bash
cp .env.example .env
# edit .env
cp storage/elasticsearch/conf/elasticsearch.yml.example storage/elasticsearch/conf/elasticsearch.yml

docker network create logstash
chmod 777 storage/elasticsearch/data  # on Ubuntu
docker-compose run --rm --name 'erin-brockovich' -e "ELASTIC_PASSWORD=changeme" elasticsearch
docker exec -it erin-brockovich bin/elasticsearch-setup-passwords auto --batch > credentials.txt
docker-compose down

cp storage/logstash/conf/logstash.yml.example storage/logstash/conf/logstash.yml
cp storage/logstash/pipeline/logstash.conf.example storage/logstash/pipeline/logstash.conf
cp storage/kibana/conf/kibana.yml.example storage/kibana/conf/kibana.yml
# edit storage/logstash/conf/logstash.yml
# replace elastic/changeme to logstash_system and its secret in credentials.txt
# edit storage/logstash/pipeline/logstash.conf
# replace changeme to elastic's secret it credentials.txt
# edit storage/kibana/conf/kibana.yml
# replace elastic/changeme to kibana_system and its secret in credentials.txt

docker-compose up
```

Wait until kibana is up and visit [http://kibana.example](http://kibana.example).
The username and password is `elastic` and its secret in `credentials.txt`.


## References

* The Complete Guide to the ELK Stack: [https://logz.io/learn/complete-guide-elk-stack/#installing-elk](https://logz.io/learn/complete-guide-elk-stack/#installing-elk)
* Installing the ELK stack on Docker: [https://logz.io/blog/elk-stack-on-docker/](https://logz.io/blog/elk-stack-on-docker/)
* Docker Logs with the ELK Stack -- Part I: [https://logz.io/blog/docker-logging/](https://logz.io/blog/docker-logging/)
* Docker Logs with the ELK Stack -- Part II: [https://logz.io/blog/docker-logging-elk-stack-part-two/](https://logz.io/blog/docker-logging-elk-stack-part-two/)
* ELK Stack: [https://www.elastic.co/what-is/elk-stack](https://www.elastic.co/what-is/elk-stack)
* How to disable paid features: [https://github.com/deviantony/docker-elk#how-to-disable-paid-features](https://github.com/deviantony/docker-elk#how-to-disable-paid-features)
* Elasticsearch repository: [https://github.com/elastic/elasticsearch](https://github.com/elastic/elasticsearch)
* Elasticsearch registry: [https://hub.docker.com/\_/elasticsearch](https://hub.docker.com/_/elasticsearch)
* Logstash repository: [https://github.com/elastic/logstash](https://github.com/elastic/logstash)
* Logstash registry: [https://hub.docker.com/\_/logstash](https://hub.docker.com/_/logstash)
* Kibana repository: [https://github.com/elastic/kibana](https://github.com/elastic/kibana)
* Kibana registry: [https://hub.docker.com/\_/kibana](https://hub.docker.com/_/kibana)
* deviantony/docker-elk: [https://github.com/deviantony/docker-elk](https://github.com/deviantony/docker-elk)
